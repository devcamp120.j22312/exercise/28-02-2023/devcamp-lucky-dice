import { Component } from "react";
import dice from "../assets/images/dice.png"
import dice1 from "../assets/images/1.png"
import dice2 from "../assets/images/2.png"
import dice3 from "../assets/images/3.png"
import dice4 from "../assets/images/4.png"
import dice5 from "../assets/images/5.png"
import dice6 from "../assets/images/6.png"

class LuckyDice extends Component {
    constructor(props){
        super(props)
        this.state = {
            number: 0,
            url: dice
        }
    }
    luckyDiceHandler = (event) => {
        const diceRandom = Math.floor(Math.random() * 6) + 1;
        this.setState({
            number: diceRandom
        })
        if(diceRandom === 1) {
            this.setState({
                url:dice1
            })
        }
        if(diceRandom === 2) {
            this.setState({
                url:dice2
            })
        }
        if(diceRandom === 3) {
            this.setState({
                url:dice3
            })
        }
        if(diceRandom === 4) {
            this.setState({
                url:dice4
            })
        }
        if(diceRandom === 5) {
            this.setState({
                url:dice5
            })
        }
        if(diceRandom === 6) {
            this.setState({
                url:dice6
            })
        }
    }

    render () {
        return(
            <>
            <button onClick={this.luckyDiceHandler}>Click Me</button>
            <p>Dice: {this.state.number}</p>
            <img src={this.state.url} width="500px" />
            </>
        )
    }
}
export default LuckyDice;